"use strict";
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var passport = require('passport');
var routes = require('./routes/index');
var http = require('http');
var User = require('./app/models/User');
var config = require('./app/config/Database');
var app = express();
app.use(morgan('dev'));
// get request parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(passport.initialize());
// connect to MongoDB
mongoose.connect(config.database);
// all environments
app.set('port', process.env.PORT || 3000);
app.engine('html', require('ejs').renderFile);
app.post('/login', routes.login);
app.post('/register', routes.register);
app.post('/getLogs', routes.getLogs);
app.post('/startLog', routes.startLog);
app.post('/endLog', routes.endLog);
http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});
//# sourceMappingURL=app.js.map