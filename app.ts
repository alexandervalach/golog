﻿import express = require('express');
import mongoose = require('mongoose');
import bodyParser = require('body-parser');
import morgan = require('morgan');
import passport = require('passport');
import routes = require('./routes/index');
import http = require('http');
import path = require('path');

var User = require('./app/models/User');
var config = require('./app/config/Database');
var app = express();

app.use(morgan('dev'));

// get request parameters
app.use(bodyParser.urlencoded({ extended: false }));

app.use(passport.initialize());
// connect to MongoDB
mongoose.connect(config.database);

// all environments
app.set('port', process.env.PORT || 3000);
app.engine('html', require('ejs').renderFile);

app.post('/login', routes.login);
app.post('/register', routes.register);
app.post('/getLogs', routes.getLogs);
app.post('/startLog', routes.startLog);
app.post('/endLog', routes.endLog);

http.createServer(app).listen(app.get('port'),function () {
    console.log('Express server listening on port ' + app.get('port'));
});