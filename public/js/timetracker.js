$(document).ready(function () {
    var taskId, json, start = false, endDate;
    countTime();
    
    /**
      * Triggers issue timer after startTracker button is clicked
      */
      $("form#form").on('click', '#startTracker', function (e) {
        e.preventDefault();
        
        var jsonStart = {};
        jsonStart.token = sessionStorage.getItem('GoLogSessionToken');
        jsonStart.startLog = Date();
        
        console.log(jsonStart);
        
        $(this).attr('id', 'stopTracker');
        $(this).removeClass('red');
        $(this).addClass('yellow');
        $(this).text('Stop');
        start = true;
        
        $.ajax({
            type: 'POST',
            url: 'http://localhost:3000/startLog',
            dataType: 'json',
            async: true,
            data: jsonStart,
            success: true
        });
    });
    
    /**
	  * Stops issue timer after stopTracker button is clicked
	  */
	  $("form#form").on('click', '#stopTracker', function (e) {
        e.preventDefault();
        start = false;
      	endDate = Date();
        
        // Switches stopTracker button to startTracker button
        $(this).attr('id', 'startTracker');
        $(this).removeClass('yellow');
        $(this).addClass('red');
        $(this).text('Start');
    });
    
    $("form#form").on('click', '#commit', function (e) {
        e.preventDefault();
        if (!start) {
            json = "";
            
            $(this).attr('id', 'save');
            $(this).text("Done");
            
            $("#form").append("<textarea id=\"commitMsg\"></textarea>");
        }
    });
    
    
    $("form#form").on('click', '#save', function (e) {
        e.preventDefault();
        var jsonEnd = {};
        jsonEnd.token = sessionStorage.getItem('GoLogSessionToken');
        jsonEnd.endLog = endDate;
        console.log(jsonEnd);
        
        $(this).attr('id', 'commit');
        $(this).text("Save");
        $("#hours").text("00");
        $("#min").text("00");
        $("#sec").text("00");
        
        
        var text = $('#commitMsg').val();
        
        $.ajax({
            type: 'POST',
            url: 'http://localhost:3000/endLog',
            dataType: 'json',
            async: true,
            data: jsonEnd,
            success: true
        });
        //$("#collect1").append(jso)
        $("#commitMsg").detach();
    });
    
    
    /**
* Triggers and stops time interval
*/
function countTime() {
        var timeInterval;
        
        $("form#form").on('click', '#startTracker', function (e) {
            var startSeconds = parseInt($("#sec").text());
            var startMinutes = parseInt($("#min").text());
            var startHours = parseInt($("#hours").text());
            
            e.preventDefault();
            timeInterval = setInterval(function () {
                // Add a leading zero to seconds value
                if (startMinutes == 59 && startSeconds == 59) {
                    startMinutes = 0;
                } else if (startSeconds == 59) {
                    startMinutes++;
                }
                
                if (startSeconds == 59) {
                    startSeconds = 0
                } else {
                    startSeconds++;
                }
                
                if (startMinutes == 59 && startSeconds == 59) {
                    startHours++
                } else {
                    startHours;
                }
                
                $("#sec").html((startSeconds < 10 ? "0" : "") + startSeconds);
                $("#min").html((startMinutes < 10 ? "0" : "") + startMinutes);
                $("#hours").html((startHours < 10 ? "0" : "") + startHours);
            }, 1000);
        });
        
        $("form#form").on('click', '#stopTracker', function (e) {
            e.preventDefault();
            clearInterval(timeInterval);
        });
    }
});