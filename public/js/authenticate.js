$(document).ready( function () {
	var email, password;

	$.ajaxPrefilter(function( options, originalOptions, jqXHR ) {
	    options.async = true;
	});

	// prevent button from default behaviour
	$("#authenticate").bind('click', function(e) {;
		e.preventDefault();
		email = $("input#email").val();
		password = $("input#password").val();
        
        var send = {}
        send.email = email;
        send.password = password;
		console.log(send);

		$.ajax({
			type: 'POST',
			url: 'http://localhost:3000/login',
			dataType: 'json',
			async: true,
			data: send,
            success: function (data) {
                console.log(data.token);
                sessionStorage.setItem("GoLogSessionToken", data.token);
                window.location.assign('user.html');
            }		
		});
	});
});

/*
Materialize.toast('I am a toast!', 4000)
*/