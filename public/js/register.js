$(document).ready( function () {
	$.ajaxPrefilter(function( options, originalOptions, jqXHR ) {
	    options.async = true;
	});

	// prevent button from default behaviour
	var email, password, firstName, lastName;

	$("#register").bind('click', function(e) {
		e.preventDefault();
		email = $("input#reg_email").val();
		password = $("input#reg_password").val();
		firstName = $("input#first_name").val();
		lastName = $("input#last_name").val();

        var send = {};
        send.firstName = firstName;
        send.lastName = lastName, 
        send.email = email;
        send.password = password;

		console.log(send);

		$.ajax({
			type: 'POST',
			url: 'http://localhost:3000/register',
			dataType: 'json',
			data: send,
			success: function(data) { console.log(data) }		
		});
	});
});