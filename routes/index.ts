﻿/*
 * GET home page.
 */
import express = require('express');
import passport = require('passport');
import config = require('../app/config/Database')
import jwt = require('jwt-simple');
require('../app/config/Passport')(passport);
import User = require('../app/models/User');
import Log = require('../app/models/Log');

export function login(req: express.Request, res: express.Response) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");

    User.findOne({
        email: req.body.email
    }, function (err, user) {
        if (err)
            throw err
        if (!user)
            res.send({ success: false })
        else {
            user.comparePassword(req.body.password, function (err, isMatch) {
                if (isMatch && !err) {
                    var token = jwt.encode(user, config.secret);
                    res.json({ success: true, token: 'JWT' + token });
                } else {
                    res.send({success: false});
                }
            });
        }
    });
};

export function register(req: express.Request, res: express.Response) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");

    console.log(req.body);

    if (!req.body) {
        res.json({ success: false });
        res.end();
        return;
    }
    if (!req.body.firstName || !req.body.password)
        res.json({ success: false });
    else {
        var newUser = new User({
            name: {
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                nick: req.body.nick ? req.body.nick: ""
            },
            email: req.body.email,
            password: req.body.password,
            privileges: req.body.privileges ? 'ADMIN' : 'STANDARD'
        });

        newUser.save(function (err) {
            if (err)
                return res.json({ success: false });
            res.json({ success: true });
        });
    }
};

export function getLogs(req: express.Request, res: express.Response) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");

    var token = req.body.token;
    if (token) {
        var decoded = jwt.decode(token, config.secret);
        User.findOne({ email: decoded.email }, function (err, user) {
            if (err)
                throw err;
            if (!user)
                return res.status(403).send({ success: false });
            else {
                var data;
                Log.find({ "useremail": decoded.email }, function (err, logs) {
                    if (err)
                        res.json({ "success": false });
                    else
                        res.json({ "success": true, data: logs });
                });
            }
        });
    }
};

export function startLog(req: express.Request, res: express.Response) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");

    var token = req.body.token;
    if (token) {
        var decoded = jwt.decode(token, config.secret);
        User.findOne({ email: decoded.email }, function (err, user) {
            if (err)
                throw err;
            if (!user)
                return res.status(403).send({ success: false });
            else {
                var log = new Log({
                    startTime: req.body.data.startTime,
                    userEmail: decoded.email
                });
                log.save(function (err, savedLog) {
                    if (err)
                        res.json({ success: false });
                    else
                        res.json({ success: true, data: savedLog });
                });
            }
        });
    }
};

export function endLog(req: express.Request, res: express.Response) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");

    var token = req.body.token;
    if (token) {
        var decoded = jwt.decode(token, config.secret);
        User.findOne({ email: decoded.email }, function (err, user) {
            if (err)
                throw err;
            if (!user)
                return res.status(403).send({ success: false });
            else {
                Log.findById(req.body.data._id, function (err, log) {
                    log.description = req.body.data.description;
                    log.endTime = req.body.data.endTime;
                    log.save(function (err, savedLog) {
                        if (err)
                            res.json({ success: false });
                        else
                            res.json({ success: true, data: savedLog });
                    });
                }
            }
        });
    }
};
