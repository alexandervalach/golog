﻿import passport = require('passport');
import passportJWT = require('passport-jwt');

var User = require('../models/User');
var config = require('./Database');
var JWTStrategy = passportJWT.Strategy;

module.exports = function (passport) {
    var opts: passportJWT.StrategyOptions = {
        algorithms: null,
        audience: null,
        ignoreExpiration: null,
        issuer: null,
        jwtFromRequest: passportJWT.ExtractJwt.fromAuthHeader(),
        passReqToCallback: null,
        secretOrKey: config.secret
    }
    passport.use(new JWTStrategy(opts, function (payload, done) {
        User.findOne({ id: payload.id }, function (err, user) {
            if (err)
                return done(err);
            if (user)
                done(user);
            else
                done(null, false);
        });
    }));
}