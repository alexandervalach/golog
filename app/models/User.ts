﻿import mongoose = require('mongoose');
import Schema = mongoose.Schema;
import bcrypt = require('bcrypt');

var UserSchema = new Schema({
    name: {
        first: {
            type: String
        },
        last: {
            type: String
        },
        nick: {
            type: String
        }
    },
    email: {
        type: String
    },
    password: {
        type: String
    },
    privileges: {
        type: String,
        enum: ['ADMIN', 'STANDARD'],
        default: 'STANDARD'
    }
});

UserSchema.pre('save', function (next) {
    var user = this;
    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, function (err, salt) {
            if (err)
                return next(err);
            bcrypt.hash(user.password, salt, function (err, hash) {
                if (err)
                    next(err);
                user.password = hash;
                next();
            });
        });
    } else {
        return next();
    }
});

UserSchema.methods.comparePassword = function (passw, cb) {
    bcrypt.compare(passw, this.password, function (err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
}

var User = mongoose.model('User', UserSchema);
export = User;